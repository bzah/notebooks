# C4I Use Cases as Jupyter Notebooks

***A collection of Jupyter Notebooks implementing some Use Cases using [ICCLIM](https://github.com/cerfacs-globc/icclim).***

Please note that all notebooks are now only compatible with icclim 5.x. 

The current version of icclim is [ICCLIM 5.0.0RC1](https://github.com/cerfacs-globc/icclim/tree/develop).

icclim 5.0.0rc1 can be installed using pip: `pip install icclim `

The notebooks in this repository can be executed in a [Climate4Impact v2](https://dev.climate4impact.eu/c4i-frontend/). Please [Sign Up](https://dev.climate4impact.eu/c4i-frontend/signup) to C4I as an ***alpha*** tester to do so!

Follow the descriptions in each notebook to identify the datasets used for the analysis and sarch them in C4I.


